﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Exam12019.Models;
using Exam12019.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Exam12019.API
{
	[Route("api/reservation")]
    [ApiController]
    [Produces("application/json")]
    public class ReservationApiController : Controller
	{
		private readonly AppSettingsService _Env;
		private readonly ReservationCacheService _ReservationCache;
		private readonly RestaurantCacheService _RestaurantCache;

		public ReservationApiController(AppSettingsService env, ReservationCacheService reservationCache, RestaurantCacheService restaurantCache)
		{
			_Env = env;
			_ReservationCache = reservationCache;
			_RestaurantCache = restaurantCache;
		}

        /// <summary>
        /// Menampilkan semua data dari reservasi.
        /// </summary>
        /// <returns></returns>
        public async Task<List<ReservationModel>> GetAsync()
        {
            return await _ReservationCache.GetReservationAsync();
        }

        /// <summary>
        /// Mendapatkan detail dari reservasi berdasarkan ID-nya
        /// </summary>
        /// <param name="reservationID"></param>
        /// <returns></returns>
		[HttpGet("{reservationID:Guid}", Name = "get-reservation")]
		public async Task<ActionResult<ReservationModel>> GetAsync(Guid reservationID)
		{
			var listReservation = await _ReservationCache.GetReservationAsync();
			var getReservation = listReservation
				.Where(reservation => reservation.ReservationID == reservationID)
				.FirstOrDefault();

			if (getReservation == null)
			{
				return NotFound("ID Reservation tidak ditemukan.");
			}

			return getReservation;
		}
        /// <summary>
        /// Menambahkan Resevasi kedalam ReservasiCacheService
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
		// POST api/<controller>
		[HttpPost(Name = "insert-reservation")]
		public async Task<ActionResult<bool>> PostAsync([FromBody]ReservationBody value)
		{
			var apiKey = Request.Headers["api-key"];
			if (string.IsNullOrEmpty(apiKey) || apiKey != _Env.ReservationApiKey)
			{
				return BadRequest("Invalid API Key");
			}

			if (await IsRestaurantIDAsync(value.RestaurantID) == false)
			{
				return BadRequest("Invalid Restaurant ID");
			}

			await InsertToCacheAsync(value);
			return true;
		}

		/// <summary>
		/// Masukkan reservation body value ke reservation cache.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		private async Task InsertToCacheAsync(ReservationBody value)
		{
			await _ReservationCache.CreateReservationAsync(new ReservationModel
			{
				ReservationID = Guid.NewGuid(),
				Name = value.Name,
				Email = value.Email,
				ReservationTime = value.ReservationTime.ToUniversalTime(),
				RestaurantID = value.RestaurantID
			});
		}

		/// <summary>
		/// Return true apabila ada restaurantID
		/// </summary>
		/// <param name="restaurantID"></param>
		/// <returns></returns>
		private async Task<bool> IsRestaurantIDAsync(Guid restaurantID)
		{
			var listRestaurant = await _RestaurantCache.GetRestaurantAsync();
			var index = listRestaurant
				.FindIndex(restaurant => restaurant.RestaurantID == restaurantID);

			if (index == -1)
			{
				return false;
			}

			return true;
		}

		/// <summary>
		/// Merupakan form body untuk reservation.
		/// </summary>
		public class ReservationBody
		{
			[Required]
			[StringLength(255)]
			public string Name { get; set; }
			[Required]
			[StringLength(255)]
			public string Email { get; set; }
			[Required]
			public DateTime ReservationTime { get; set; }
			[Required]
			public Guid RestaurantID { get; set; }
		}
	}
}
