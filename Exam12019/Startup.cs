using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exam12019.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;

namespace Exam12019
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            // Tambahkan DistributedMemoryCache.
            services.AddDistributedMemoryCache();

            // Tambahkan appsettings.json ke AppSettingsService.
            services.Configure<AppSettingsService>(Configuration);
            services.AddScoped(di => di.GetRequiredService<IOptionsMonitor<AppSettingsService>>().CurrentValue);

            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = "localhost";
                options.InstanceName = "Exam2";
            });

            // Menambahkan service Autentikasi.
            services
                .AddAuthentication("ExamAuth")
                .AddCookie("ExamAuth", option =>
                {
                    option.LoginPath = "/auth/login";
                    option.LogoutPath = "/auth/logout";
                    option.AccessDeniedPath = "/auth/denied";
                });

            //Menambahkan restaurant service agar dapat disimpan
            services.AddScoped<RestaurantCacheService>();

            //Menambahkan reservation service agar dapat disimpan
            services.AddScoped<ReservationCacheService>();

            // Menambahkan service HTTPClientFactory.
            services.AddHttpClient();

            // Menambahkan Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Exam2", Version = "v1" });
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();
            app.UseCookiePolicy();

            //buat auth
            app.UseAuthentication();

            // Menambah Swagger.
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Exam2");
                c.DisplayOperationId();
            });

            app.UseMvc();
        }
    }
}
