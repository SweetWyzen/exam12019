﻿using Exam12019.Models;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Services {
    public class ReservationCacheService {

        private readonly IDistributedCache _Cache;

        public ReservationCacheService(IDistributedCache cache)
        {
            _Cache = cache;
        }

        public async Task<bool> CreateReservationAsync(ReservationModel reservationModel)
        {
            var getReservation = await GetReservationAsync();

            getReservation.Add(reservationModel);

            var convertJson = JsonConvert.SerializeObject(getReservation);
            await _Cache.SetStringAsync("Reservation", convertJson);
            return true;

        }

        public async Task<List<ReservationModel>> GetReservationAsync()
        {
            var getJson = await _Cache.GetStringAsync("Reservation");
            if (string.IsNullOrEmpty(getJson))
            {
                return new List<ReservationModel>();
            }

            var getList = JsonConvert.DeserializeObject<List<ReservationModel>>(getJson);
            return getList;
        }

    }
}
