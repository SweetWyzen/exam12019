﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Services
{
    /// <summary>
    /// class buat appsetting.json
    /// </summary>
	public class AppSettingsService
	{
		public string AdminUsername { get; set; }
		public string AdminPassword { get; set; }
		public string ReservationApiKey { get; set; }
		public string ReservationApiAddress { get; set; }
	}
}
