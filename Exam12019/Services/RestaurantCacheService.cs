﻿using Exam12019.Models;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Services {
    public class RestaurantCacheService {

        private IDistributedCache _Cache;

        public RestaurantCacheService(IDistributedCache cache)
        {
            this._Cache = cache;
        }
        /// <summary>
        /// untuk ambil semua data dari redis database
        /// </summary>
        /// <returns></returns>
        public async Task<List<RestaurantModel>> GetRestaurantAsync()
        {
            var getJson = await _Cache.GetStringAsync("Restaurant");
            if (string.IsNullOrEmpty(getJson))
            {
                return new List<RestaurantModel>();
            }

            var getList = JsonConvert.DeserializeObject<List<RestaurantModel>>(getJson).ToList();

            return getList;
        }
        /// <summary>
        /// post / create new restaurant ke dalam redis
        /// </summary>
        /// <param name="restaurantModel"></param>
        /// <returns></returns>
        public async Task<bool> CreateRestaurantAsync(RestaurantModel restaurantModel)
        {
            var getRestaurant = await GetRestaurantAsync();

            getRestaurant.Add(restaurantModel);

            var convertJson = JsonConvert.SerializeObject(getRestaurant);
            await _Cache.SetStringAsync("Restaurant", convertJson);
            return true;

        }
        /// <summary>
        /// update restaurant ke redis
        /// </summary>
        /// <param name="restaurantModel"></param>
        /// <returns></returns>
        public async Task<bool> UpdateRestaurant(RestaurantModel restaurantModel)
        {
            var getRestaurant = await GetRestaurantAsync();
            if (getRestaurant.FirstOrDefault() == null)
            {
                return false;
            }
            //dapatkan index yang sesuai di list
            var getIndex = getRestaurant.FindIndex(Q => Q.RestaurantID == restaurantModel.RestaurantID);
            if (getIndex == -1)
            {
                return false;
            }
            getRestaurant[getIndex] = restaurantModel;

            var convertJson = JsonConvert.SerializeObject(getRestaurant);
            await _Cache.SetStringAsync("Restaurant", convertJson);
            return true;
        }
        /// <summary>
        /// delete restaurant dari redis
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteRestaurant(Guid id)
        {
            var getRestaurant = await GetRestaurantAsync();
            if (getRestaurant.FirstOrDefault() == null)
            {
                return false;
            }

            var valid = getRestaurant.Where(Q => Q.RestaurantID == id).FirstOrDefault();
            if (valid == null)
            {
                return false;
            }

            getRestaurant.Remove(valid);
            var convertJson = JsonConvert.SerializeObject(getRestaurant);
            await _Cache.SetStringAsync("Restaurant", convertJson);
            return true;

        }

    }
}
