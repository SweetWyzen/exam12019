﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Models {
    public class RestaurantModel {
        
        public Guid RestaurantID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }

    }
}
