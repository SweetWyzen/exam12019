﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Models
{
	public class ReservationModel
	{
		public Guid ReservationID { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
		public DateTime ReservationTime { get; set; }
		public Guid RestaurantID { get; set; }
	}
}
