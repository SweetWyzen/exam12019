﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages
{
    [Authorize(Roles = "Administrator")]
    public class IndexModel : PageModel
    {
        /// <summary>
        /// ke page Index restaurant
        /// </summary>
        /// <returns></returns>
        public void OnGet()
        {
            
        }
    }
}
