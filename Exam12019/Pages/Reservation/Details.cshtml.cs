using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Models;
using Exam12019.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;

namespace Exam12019.Pages.Reservation
{
    public class DetailsModel : PageModel
    {
		[BindProperty(SupportsGet = true)]
		public Guid ReservationID { set; get; }

		public ReservationModel Reservation;
		public RestaurantModel Restaurant;

		private readonly IHttpClientFactory _HttpClient;
		private readonly AppSettingsService _Env;
		private readonly RestaurantCacheService _RestaurantCache;
		private readonly ReservationCacheService _ReservationCache;

        /// <summary>
        /// Constructor dari DetailsModel.
        /// </summary>
        /// <param name="httpClient"></param>
        /// <param name="env"></param>
        /// <param name="restaurantCache"></param>
        /// <param name="reservationCache"></param>
		public DetailsModel(IHttpClientFactory httpClient, AppSettingsService env, RestaurantCacheService restaurantCache, ReservationCacheService reservationCache)
		{
			_HttpClient = httpClient;
			_Env = env;
			_RestaurantCache = restaurantCache;
            _ReservationCache = reservationCache;

        }

        /// <summary>
        /// Merupakan method untuk get page index ini.
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnGetAsync()
        {
            if (await IsReservationIDAsync() == false)
            {
                return NotFound("ID Reservasi tidak ditemukan.");
            }

			var client = _HttpClient.CreateClient();
			var response = await client.GetAsync(_Env.ReservationApiAddress + ReservationID);

			response.EnsureSuccessStatusCode();

			var contentJson = await response.Content.ReadAsStringAsync();
			Reservation = JsonConvert.DeserializeObject<ReservationModel>(contentJson);
			await SetRestaurant(Reservation);

            return Page();
		}

        /// <summary>
        /// Untuk set nilai value restaurant.
        /// </summary>
        /// <param name="reservation"></param>
        /// <returns></returns>
		private async Task SetRestaurant(ReservationModel reservation)
		{
			var allRestaurant = await _RestaurantCache.GetRestaurantAsync();
			Restaurant = allRestaurant
				.Where(restaurant => restaurant.RestaurantID == reservation.RestaurantID)
				.FirstOrDefault();
		}

        /// <summary>
        /// Mengecek apakah id reservasi ditemukan atau tidak.
        /// </summary>
        /// <returns></returns>
        private async Task<bool> IsReservationIDAsync()
        {
            var listReservation = await _ReservationCache.GetReservationAsync();
            var index = listReservation.FindIndex(reservation => reservation.ReservationID == ReservationID);
            if (index == -1)
            {
                return false;
            }
            return true;
        }
    }
}