using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Models;
using Exam12019.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;

namespace Exam12019.Pages.Reservation
{
    public class IndexModel : PageModel
    {
        private readonly IHttpClientFactory _HttpClient;
        public readonly AppSettingsService Env;

        public List<ReservationModel> ReservationList;

        [BindProperty]
        public Guid ReservationID { get; set; }

        /// <summary>
        /// Constructor dari IndexModel.
        /// </summary>
        /// <param name="httpClient"></param>
        /// <param name="env"></param>
        public IndexModel(IHttpClientFactory httpClient, AppSettingsService env)
        {
            _HttpClient = httpClient;
            Env = env;
        }

        /// <summary>
        /// Mendapatkan seluruh list reservasi.      
        /// </summary>
        /// <returns></returns>
        public async Task OnGet()
        {   
            var client = _HttpClient.CreateClient();
            var response = await client.GetAsync(Env.ReservationApiAddress);
            response.EnsureSuccessStatusCode();
            ReservationList = JsonConvert.DeserializeObject<List<ReservationModel>>(await response.Content.ReadAsStringAsync());
        }

        /// <summary>
        /// Redirect to halaman details berdasarkan ID.
        /// </summary>
        /// <returns></returns>
        public IActionResult OnPost()
        {
            return RedirectToPage("/Reservation/Details" , new { reservationID = ReservationID });
        }
    }
}