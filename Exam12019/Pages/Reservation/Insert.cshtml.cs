using Exam12019.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.ComponentModel.DataAnnotations;
using System.Net.Http;
using System.Threading.Tasks;
using static Exam12019.API.ReservationApiController;

namespace Exam12019.Pages.Reservation {
    public class InsertModel : PageModel {

        [BindProperty]
        public ReservationForm Form { get; set; }

        public readonly RestaurantCacheService RestaurantCache;
        private readonly IHttpClientFactory _HttpClient;
        private readonly AppSettingsService _Env;

        /// <summary>
        /// Constructor dari InsertModel.
        /// </summary>
        /// <param name="restaurantCache"></param>
        /// <param name="httpClient"></param>
        /// <param name="env"></param>
        public InsertModel(RestaurantCacheService restaurantCache, IHttpClientFactory httpClient, AppSettingsService env)
        {
            RestaurantCache = restaurantCache;
            _HttpClient = httpClient;
            _Env = env;
        }

        public void OnGet()
        {
        }

        /// <summary>
        /// Method ketika user menambahkan reservasi.
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnPostAsync()
        {
            var client = _HttpClient.CreateClient();
            client.DefaultRequestHeaders.Add("api-key", _Env.ReservationApiKey);
            var response = await client.PostAsJsonAsync(_Env.ReservationApiAddress, new ReservationBody
            {
                Name = Form.Name,
                Email = Form.Email,
                ReservationTime = Form.ReservationTime,
                RestaurantID = Form.RestaurantID
            });
            response.EnsureSuccessStatusCode();
            return RedirectToPage("/Restaurant/Index");
        }

        /// <summary>
        /// Merupakan sebuah form untuk reservasi.
        /// </summary>
        public class ReservationForm {
            [Required]
            [StringLength(255)]
            public string Name { get; set; }
            [Required]
            [StringLength(255)]
            public string Email { get; set; }
            [Required]
            public DateTime ReservationTime { get; set; }
            [Required]
            public Guid RestaurantID { get; set; }
        }
    }
}