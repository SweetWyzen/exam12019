using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exam12019.Models;
using Exam12019.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages.Restaurant
{
    [Authorize(Roles = "Administrator")]
    public class DeleteModel : PageModel
    {
        [BindProperty(SupportsGet = true)]
        public Guid RestaurantID { set; get; }


        [BindProperty(SupportsGet = true)]
        public RestaurantModel FormRestaurant { set; get; }

        private RestaurantCacheService _RestaurantCacheService;
        /// <summary>
        /// Ini konstruktor dari deleteModel.
        /// </summary>
        /// <param name="restaurantCacheService"></param>
        public DeleteModel(RestaurantCacheService restaurantCacheService)
        {
            this._RestaurantCacheService = restaurantCacheService;
        }

        /// <summary>
        /// dapatkan list restaurant dari cache
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnGet()
        {
            if (await CheckID() == false)
            {
                return NotFound("Restaurant not found :(");
            }

            var getList = await _RestaurantCacheService.GetRestaurantAsync();
            FormRestaurant = getList.Where(Q => Q.RestaurantID == RestaurantID).FirstOrDefault();
            if (FormRestaurant == null)
            {
                return NotFound("Restaurant yang dicari tidak tersedia");
            }

            return Page();
        }

        /// <summary>
        /// untuk delete data ke cache
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnPost()
        {
            var success = await _RestaurantCacheService.DeleteRestaurant(RestaurantID);
            if (success == false)
            {
                TempData["ErrorMessage"] = "Failed to Delete";
                return Page();
            }

            return RedirectToPage("/Restaurant/Index");

        }
        /// <summary>
        /// ini untuk cek url id ada atau tidak di database
        /// </summary>
        /// <returns></returns>
        public async Task<bool> CheckID()
        {
            var getList = await _RestaurantCacheService.GetRestaurantAsync();
            var getIndex = getList.FindIndex(Q => Q.RestaurantID == RestaurantID);
            if (getIndex == -1)
            {
                return false;
            }

            return true;
        }
    }
}