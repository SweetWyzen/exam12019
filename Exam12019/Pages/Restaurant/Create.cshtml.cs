using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Exam12019.Models;
using Exam12019.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages.Restaurant
{
    [Authorize(Roles = "Administrator")]
    public class CreateModel : PageModel {
        
        private readonly RestaurantCacheService _RestaurantCacheService;

        [BindProperty]
        public CreateForm FormRestaurant { set; get; } = new CreateForm();
        /// <summary>
        /// ini conscrutor dari create model
        /// </summary>
        /// <param name="RestaurantCacheService"></param>
        public CreateModel(RestaurantCacheService RestaurantCacheService)
        {
            this._RestaurantCacheService = RestaurantCacheService;
        }

        public void OnGet()
        {
        }

        /// <summary>
        /// menerima input dari form dan diteruskan ke cache buat di insert
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnPost()
        {
            var success = await _RestaurantCacheService.CreateRestaurantAsync(new RestaurantModel{
                RestaurantID = Guid.NewGuid(),
                Name = FormRestaurant.Name,
                Address = FormRestaurant.Address
            });
            if (success == false)
            {
                TempData["ErrorMessage"] = "Failed to create";
                return Page();
            }

            return RedirectToPage("/Restaurant/Index");
        }
        /// <summary>
        /// kelas buat nampung inputan
        /// </summary>
        public class CreateForm{
            [Required]
            [StringLength(255)]
            public string Name { get; set; }

            [Required]
            [StringLength(2000)]
            public string Address { get; set; }
        }

    }
}