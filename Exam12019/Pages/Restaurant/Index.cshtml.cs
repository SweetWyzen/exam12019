using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exam12019.Models;
using Exam12019.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages.Restaurant
{
    [Authorize(Roles = "Administrator")]
    public class IndexModel : PageModel
    {   
        private RestaurantCacheService _RestaurantCacheService;

        [BindProperty(SupportsGet = true)]
        public List<RestaurantModel> RestaurantList { set; get; }

        [BindProperty(SupportsGet = true)]
        public bool IsEmpty { set; get; }
        /// <summary>
        /// Ini constructor dari Index restaurant 
        /// </summary>
        /// <param name="restaurantCacheService"></param>
        public IndexModel(RestaurantCacheService restaurantCacheService)
        {
            this._RestaurantCacheService = restaurantCacheService;
        }
        /// <summary>
        /// buat dapetin data dari cache
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnGet()
        {
            var getList = await _RestaurantCacheService.GetRestaurantAsync();
            if (getList.FirstOrDefault() == null)
            {
                IsEmpty = true;
            }
            IsEmpty = false;
            RestaurantList = getList;

            return Page();
        }
    }
}