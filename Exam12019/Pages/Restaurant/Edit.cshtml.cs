using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Exam12019.Models;
using Exam12019.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages.Restaurant
{
    [Authorize(Roles = "Administrator")]
    public class EditModel : PageModel
    {
        [BindProperty(SupportsGet =true)]
        public Guid RestaurantID { set; get; }

        [BindProperty(SupportsGet = true)]
        public EditForm FormRestaurant { set; get; }

        private RestaurantCacheService _RestaurantCacheService;
        /// <summary>
        /// ini konstructor dari EditModel
        /// </summary>
        /// <param name="restaurantCacheService"></param>
        public EditModel(RestaurantCacheService restaurantCacheService)
        {
            this._RestaurantCacheService = restaurantCacheService;
        }

        /// <summary>
        /// dapat detail data dari cache berdasarkan id
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnGet()
        {
            if(await CheckID() == false)
            {
                return NotFound("Restaurant not found :(");
            }

            var getList = await _RestaurantCacheService.GetRestaurantAsync();
            var getRestaurant = getList.Where(Q => Q.RestaurantID == RestaurantID).FirstOrDefault();
            if (FormRestaurant == null)
            {
                return NotFound("Restaurant yang dicari tidak tersedia");
            }

            FormRestaurant.Name = getRestaurant.Name;
            FormRestaurant.Address = getRestaurant.Address;

            return Page();
        }
        /// <summary>
        /// mengirimkan formmodel untuk update di cache
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnPost()
        {
            var success = await _RestaurantCacheService.UpdateRestaurant(new RestaurantModel
            {
                RestaurantID = RestaurantID,
                Name = FormRestaurant.Name,
                Address = FormRestaurant.Address
            });

            if (success == false)
            {
                TempData["ErrorMessage"] = "Failed to update";
                return Page();
            }

            return RedirectToPage("/Restaurant/Index");

        }
        /// <summary>
        /// untuk cek apakah id ada di database atau tidak 
        /// </summary>
        /// <returns></returns>
        public async Task<bool> CheckID()
        {
            var getList = await _RestaurantCacheService.GetRestaurantAsync();
            var getIndex = getList.FindIndex(Q => Q.RestaurantID == RestaurantID);
            if(getIndex == -1)
            {
                return false;
            }

            return true;
        }
    }
    /// <summary>
    /// kelas buat nampung inputan
    /// </summary>
    public class EditForm {
        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        [StringLength(2000)]
        public string Address { get; set; }
    }
}