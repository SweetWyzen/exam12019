using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Exam12019.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages.Auth
{
    public class LoginModel : PageModel
    {
		private readonly AppSettingsService _Env;

		[BindProperty]
		public LoginForm Form { get; set; }

		public LoginModel(AppSettingsService env)
		{
			_Env = env;
		}

        public void OnGet()
        {

        }

		public async Task<IActionResult> OnPostAsync(string returnUrl)
		{
			if (ModelState.IsValid == false)
			{
				return Page();
			}

			if (CheckAuth() == false)
			{
				ModelState.AddModelError("Form.Password", "Password atau email salah!");
				return Page();
			}

			var id = GetClaimsIdentity();
			await SetLoginSettingsAsync(id);

			if (Url.IsLocalUrl(returnUrl))
			{
				return Redirect(returnUrl);
			}

			return RedirectToPage("/Index");
		}

		/// <summary>
		/// Untuk melogin dan mensetting expire dan persistent.
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public async Task SetLoginSettingsAsync(ClaimsIdentity id)
		{
			var principal = new ClaimsPrincipal(id);
			await HttpContext.SignInAsync("ExamAuth", principal, new AuthenticationProperties
			{
				ExpiresUtc = DateTimeOffset.UtcNow.AddYears(1),
				IsPersistent = true
			});
		}

		/// <summary>
		/// Untuk mendapatkan ClaimsIdentity.
		/// </summary>
		/// <returns></returns>
		public ClaimsIdentity GetClaimsIdentity()
		{
			var id = new ClaimsIdentity("ExamAuth");
			id.AddClaim(new Claim(ClaimTypes.Name, Form.Username));
			id.AddClaim(new Claim(ClaimTypes.NameIdentifier, Form.Username)); // user ID / username / PK
			id.AddClaim(new Claim(ClaimTypes.Role, "Administrator"));
			return id;
		}

		/// <summary>
		/// Cek apakah user benar atau salah.
		/// </summary>
		/// <returns></returns>
		private bool CheckAuth()
		{
			var adminUsername = _Env.AdminUsername;
			var adminPassword = _Env.AdminPassword;
			return Form.Username == adminUsername && 
				BCrypt.Net.BCrypt.Verify(Form.Password, adminPassword);
		}

		/// <summary>
		/// Merupakan login form.
		/// </summary>
		public class LoginForm
		{
			[Required]
			public string Username { get; set; }

			[Required]
			public string Password { get; set; }
		}
    }
}